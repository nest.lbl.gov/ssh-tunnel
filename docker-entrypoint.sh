#!/bin/bash

# If not already set, set it
: ${TUNNEL_SSH_CONFIG:=/usr/share/tunnels/config}

# Start all tunnels
~/bin/tunnel_mgr start

# Keep container running and tunnels up.
echo "Keeping tunnels up..."
while true; do
    sleep 15
    ~/bin/tunnel_mgr -q status
    if [[ $? != 0 ]]; then
        echo "Status failure..."
        ~/bin/tunnel_mgr status
        echo "Restarting..."
        ~/bin/tunnel_mgr restart
        continue
    fi
    ~/bin/tunnel_mgr -q check
    if [[ $? != 0 ]]; then
        echo "Check failure..."
        ~/bin/tunnel_mgr check
        echo "Restarting..."
        ~/bin/tunnel_mgr restart
        continue
    fi
done
